<?php

namespace App;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  protected $fillable =['name_client','genre_client'];
  protected $guarded = ['id','created_at','update_at'];
  protected $table = 'clients';


  public function document(): BelongsTo
  {
      return $this->belongsTo('App\Document');
  }

}
