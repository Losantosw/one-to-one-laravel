<?php

namespace App;
use Illuminate\Database\Eloquent\Relations\HasOne;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['type_document','number_document', 'document_client'];
    protected $guarded = ['id','created_at','update_at'];
    protected $table = 'documents';


    public function client(): HasOne
    {
        return $this->hasOne('App\Client');
    }

}
