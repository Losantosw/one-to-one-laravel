<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Cliente</title>
</head>
<body>
  <div class="container">
    <div class="">
      <form action="{{route('document.store')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

          <div class="col-md-6">
            <div class="form-group">
              <h1>Cadastrar Documento</h1>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="type_document">Tipo de Documento</label>
              <input type="text" class="form-control" id="type_document" placeholder="Tipo de Documento" name="type_document">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="number_document">Número Documento</label>
              <input type="text" class="form-control" id="number_document" placeholder="Número Documento" name="number_document">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="document_client">Id Cliente</label>
              <input type="text" class="form-control" id="document_client" name="document_client">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <button type="submit" class="btn btn-success">Salvar</button>
              <button class="btn btn-info" href="{{route('client.index')}}">Voltar</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</body>
</html>
