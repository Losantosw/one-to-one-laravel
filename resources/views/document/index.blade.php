<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Documento</title>
</head>
<body>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Id Documento</th>
          <th scope="col">Id Cliente</th>
          <th scope="col">Tipo Documento</th>
          <th scope="col">Número Documento</th>
          <th scope="col">
            <a class="btn btn-info" href="{{route('client.index')}}">Voltar ao cliente</a>
          </th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($documents as $document)
          <tr>
            <th scope="row">{{ $document->id }}</th>
            <td>{{$document->document_client }}</td>
            <td>{{$document->type_document}}</td>
            <td>{{$document->number_document}}</td>
            <td>
              <a class="btn btn-warning" href="{{ route('document.edit', $document->id) }}">Editar</a>
            </td>
            <td>
              <form method="POST" action="{{route('document.destroy', $document->id)}}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Deletar</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
</body>
</html>
