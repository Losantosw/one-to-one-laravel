<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Edit Document</title>
</head>
<body>
  <div class="container">
    <form method="post" action="{{route('document.update', $document->id)}}" enctype="multipart/form-data">
        {!! method_field('put') !!}
        {{ csrf_field() }}

        <div class="col-md-6">
          <div class="form-group">
            <h1>Cliente</h1>
          </div>
        </div>

        <!-- <input type="hidden" name="id_client" value="$client->id"> -->

        <div class="col-md-6">
          <div class="form-group">
            <h1>Edit Document</h1>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="type_document">Tipo de Documento</label>
            <input type="text" class="form-control" id="type_document" name="type_document" value="{{$document->type_document}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="number_document">Número Documento</label>
            <input type="text" class="form-control" id="number_document" name="number_document" value="{{$document->number_document}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="document_client">Id Cliente</label>
            <input type="text" class="form-control" id="document_client" name="document_client" value="{{$document->document_client}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-success">Salvar</button>
            <button class="btn btn-info" href="{{route('client.index')}}">Voltar</button>
          </div>
        </div>
    </form>
  </div>
</body>
</html>
