<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Cliente</title>
</head>
<body>
  <div class="container">
    <form method="post" action="{{route('client.update', $client->id)}}" enctype="multipart/form-data">
        {!! method_field('put') !!}
        {{ csrf_field() }}

        <div class="col-md-6">
          <div class="form-group">
            <h1>Cliente</h1>
          </div>
        </div>

        <!-- <input type="hidden" name="id_client" value="$client->id"> -->

        <div class="col-md-6">
          <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="name_client" name="name_client" value="{{$client->name_client}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="autor">Autor</label>
            <input type="text" class="form-control" id="genre_client" name="genre_client" value="{{$client->genre_client}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-success">Salvar</button>
            <button class="btn btn-info" href="{{route('client.index')}}">Voltar</button>
          </div>
        </div>
    </form>
  </div>
</body>
</html>
