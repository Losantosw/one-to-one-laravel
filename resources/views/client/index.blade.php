<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Cliente</title>
</head>
<body>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Gênero</th>
          <th scope="col">
            <a class="btn btn-success" href="{{route('client.create')}}">Adicionar Cliente</a>
            <a class="btn btn-info" href="{{route('document.index')}}">Lista Documento</a>
          </th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($clients as $client)
          <tr>
            <th scope="row">{{$client->id}}</th>
            <td>{{$client->name_client}}</td>
            <td>{{$client->genre_client}}</td>
            <td>
              <a class="btn btn-warning" href="{{ route('client.edit', $client->id) }}">Editar Cliente</a>
            </td>
            <td>
              <form method="POST" action="{{route('client.destroy', $client->id)}}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Deletar Cliente</button>
              </form>
            </td>
            <td>
              <a class="btn btn-info" href="{{ route('document.index') }}">Add Documento</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
</body>
</html>
