<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Cliente</title>
</head>
<body>
  <div class="container">
    <div class="">
      <form action="{{route('client.store')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

          <div class="col-md-6">
            <div class="form-group">
              <h1>Cadastrar Cliente</h1>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="name_client">Nome</label>
              <input type="text" class="form-control" id="name_client" placeholder="Nome do Cliente" name="name_client">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="genre_client">Gênero</label>
              <input type="text" class="form-control" id="genre_client" placeholder="Gênero do Cliente" name="genre_client">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <button type="submit" class="btn btn-success">Salvar</button>
              <button class="btn btn-info" href="{{route('client.index')}}">Voltar</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</body>
</html>
